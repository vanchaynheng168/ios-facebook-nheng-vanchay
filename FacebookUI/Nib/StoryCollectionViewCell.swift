//
//  StoryCollectionViewCell.swift
//  FacebookUI
//
//  Created by BTB_011 on 30/11/20.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var storyUsername: UILabel!
    @IBOutlet weak var storyImageProfile: UIImageView!
    @IBOutlet weak var imageStory: UIImageView!
    static var identifierCell = "cell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static func nib() -> UINib{
        return UINib(nibName: "StoryCollectionViewCell", bundle: nil)
    }
    func config(profile: UIImage, storyImage: UIImage, username: String) {
        print("in cell")
        self.storyUsername?.text = username
        self.storyImageProfile?.image = profile
        self.imageStory?.image = storyImage
    }

}
