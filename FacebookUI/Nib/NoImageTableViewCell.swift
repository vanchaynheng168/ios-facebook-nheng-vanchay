//
//  NoImageTableViewCell.swift
//  FacebookUI
//
//  Created by BTB_011 on 1/12/20.
//

import UIKit

class NoImageTableViewCell: UITableViewCell {

    static var identifierCell = "cellNoImage"
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var shares: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var caption: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static func nib() -> UINib {
        return UINib(nibName: "NoImageTableViewCell", bundle: nil)
    }
    func config(profile: UIImage, username: String, caption: String, like: String, comment: String, share: String) {
        self.profile?.image = profile
        self.userName?.text = username
        self.caption?.text = caption
        self.likes?.text = like
        self.comments?.text = comment
        self.shares?.text = share
    }
}
