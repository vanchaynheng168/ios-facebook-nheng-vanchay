//
//  TableViewCell.swift
//  FacebookUI
//
//  Created by Nheng Vanchhay on 27/11/20.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var shares: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    static let identifier = "cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "TableViewCell", bundle: nil)
    }
    func config(profile: UIImage, username: String, caption: String, imagePost: UIImage, like: String, comment: String, share: String) {
        self.profile?.image = profile
        self.userName?.text = username
        self.caption?.text = caption
        self.imagePost?.image = imagePost
        self.likes?.text = like
        self.comments?.text = comment
        self.shares?.text = share
    }
}
