//
//  User.swift
//  FacebookUI
//
//  Created by Nheng Vanchhay on 27/11/20.
//

import Foundation
import UIKit

struct User {
    var username: String
    var profile: UIImage
    
    init(username: String, profile: UIImage) {
        self.username = username
        self.profile = profile
    }
}
