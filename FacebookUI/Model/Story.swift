//
//  Story.swift
//  FacebookUI
//
//  Created by Nheng Vanchhay on 27/11/20.
//

import Foundation
import UIKit

struct Story {
    var user: String
    var imageStory: UIImage
    
    init(user: String, imageStory: UIImage) {
        self.user = user
        self.imageStory = imageStory
    }
}
