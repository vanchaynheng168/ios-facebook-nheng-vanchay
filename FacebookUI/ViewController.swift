//
//  ViewController.swift
//  FacebookUI
//
//  Created by Nheng Vanchhay on 27/11/20.
//

import UIKit

class ViewController: UIViewController {

    var storyData = [Story]()
    var story0 = Story(user: "vanchay", imageStory: UIImage(named: "hd")!)
    var story1 = Story(user: "Selena", imageStory: UIImage(named: "acdemic")!)
    var story2 = Story(user: "haly", imageStory: UIImage(named: "captain")!)
    var story3 = Story(user: "sperman", imageStory: UIImage(named: "hd")!)
    var story4 = Story(user: "Gress", imageStory: UIImage(named: "huk")!)
    var story5 = Story(user: "hany", imageStory: UIImage(named: "vnom")!)
    var story6 = Story(user: "Heriku", imageStory: UIImage(named: "superman")!)
    var story7 = Story(user: "chaly", imageStory: UIImage(named: "hd")!)
    var story8 = Story(user: "Jonh Stone", imageStory: UIImage(named: "vnom")!)
    var story9 = Story(user: "Bert", imageStory: UIImage(named: "superman")!)
    var story10 = Story(user: "Bigo Bank", imageStory: UIImage(named: "superman")!)

    var userData = [User]()
    var user = User(username: "vanchay", profile: UIImage(named: "acdemic")!)
    var user1 = User(username: "Selena", profile: UIImage(named: "superman")!)
    var user2 = User(username: "Vancelery", profile: UIImage(named: "captain")!)
    var user3 = User(username: "Sal", profile: UIImage(named: "huk")!)
    var user4 = User(username: "hiya", profile: UIImage(named: "acdemic")!)
    var user5 = User(username: "karisa", profile: UIImage(named: "4k")!)
    var user6 = User(username: "lika", profile: UIImage(named: "vnom")!)
    var user7 = User(username: "bang", profile: UIImage(named: "superman")!)
    
    var postData = [Post]()
    let post = Post(user: "vanchay", caption: "Swift is a powerful and intuitive programming language for macOS, iOS, watchOS, tvOS and beyond. Writing Swift code is interactive and fun, the syntax is concise yet expressive, and Swift includes modern features developers love. Swift code is safe by design, yet also produces software that runs lightning-fast.", image: "", amountLike: "200", amountComment: "300", amountShare: "100")
    let post1 = Post(user: "Selena", caption: "Good after noon", image: "superman", amountLike: "500", amountComment: "300", amountShare: "100")
    let post2 = Post(user: "Vancelery", caption: "Note the “Automatic” checkbox. If you select it Interface Builder wakes up and realizes .Note the “Automatic” checkbox. If you select it Interface Builder wakes up and realizes .Note the “Automatic” checkbox. If you select it Interface Builder wakes up and realizes .Note the “Automatic” checkbox. If you select it Interface Builder wakes up and realizes .Note the “Automatic” checkbox. If you select it Interface Builder wakes up and realizes .Note the “Automatic” checkbox. If you select it Interface Builder wakes up and realizes .", image: "captain", amountLike: "5000", amountComment: "3000", amountShare: "1000")
    let post3 = Post(user: "007", caption: "Good morning", image: "4k", amountLike: "200", amountComment: "3070", amountShare: "100")
    let post4 = Post(user: "Jam sbon", caption: "Good morning", image: "superman", amountLike: "2070", amountComment: "3700", amountShare: "1000")
    let post5 = Post(user: "Jonh Bert", caption: "Good morning", image: "4k", amountLike: "2500", amountComment: "3070", amountShare: "1800")
    let post6 = Post(user: "kroma", caption: "Good morning", image: "superman", amountLike: "2700", amountComment: "3800", amountShare: "900")
    let post7 = Post(user: "stev", caption: "From its earliest conception, Swift was built to be fast. Using the incredibly high-performance LLVM compiler technology, Swift code is transformed into optimized native code that gets the most out of modern hardware. The syntax and standard library have also been tuned to make the most obvious way to write your code also perform the best whether it runs in the watch on your wrist or across a cluster of servers.", image: "", amountLike: "2000", amountComment: "3000", amountShare: "900")
    
//    @IBOutlet weak var logo: UIBarButtonItem!
    @IBOutlet weak var story: UICollectionView!
    @IBOutlet weak var table: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBar = self.navigationController?.navigationBar
        navBar?.barTintColor = UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "fb-logo"), style: .plain, target: self, action: nil)
        
//        
        table.register(TableViewCell.nib(), forCellReuseIdentifier: TableViewCell.identifier)
        table.register(NoImageTableViewCell.nib(), forCellReuseIdentifier: NoImageTableViewCell.identifierCell)
        story.register(StoryCollectionViewCell.nib(), forCellWithReuseIdentifier: StoryCollectionViewCell.identifierCell)
        
        storyData = [story5,story6,story0,story2,story3,story4,story7,story8,story9,story10,story1]
        userData.append(contentsOf: [user, user1, user2, user3, user4, user5, user6, user7])
        postData.append(contentsOf: [post, post1, post2, post3, post4, post5, post6, post7])
    }
    
}
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let cellNoImage = tableView.dequeueReusableCell(withIdentifier: NoImageTableViewCell.identifierCell, for: indexPath) as! NoImageTableViewCell
        
        let username = userData[indexPath.row].username
        let profile = userData[indexPath.row].profile
        let caption = postData[indexPath.row].caption
        let imagePost = UIImage(named: postData[indexPath.row].image)
        let like = postData[indexPath.row].amountLike + " likes"
        let comment = postData[indexPath.row].amountComment + " comments"
        let share = postData[indexPath.row].amountShare + " shares"
        
        if let imagePost = imagePost {
            cell.config(profile: profile, username: username, caption: caption, imagePost: imagePost, like: like, comment: comment, share: share)
            return cell
        }else{
            cellNoImage.config(profile: profile, username: username, caption: caption, like: like, comment: comment, share: share)
            return cellNoImage
        }
    }
 
}
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storyData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryCollectionViewCell.identifierCell, for: indexPath) as! StoryCollectionViewCell
        let profile = storyData[indexPath.row].imageStory
        let storyImage = storyData[indexPath.row].imageStory
        let username = storyData[indexPath.row].user
        cell.config(profile: profile, storyImage: storyImage, username: username)
        
        return cell
    }
    //itemsize
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 50)
    }
}
